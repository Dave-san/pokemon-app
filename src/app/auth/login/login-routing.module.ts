import { NgModule }       from '@angular/core';
import { RouterModule }   from '@angular/router';
import { AuthService }    from '../auth.service';
import { LoginComponent } from './login.component';
import { AuthGuardGuard } from '../../guard/auth-guard.guard';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'login', component: LoginComponent }
    ])
  ],
  exports: [
    RouterModule
  ],
  providers: [
    AuthGuardGuard,
    AuthService
    
  ]
})
export class LoginRoutingModule {}