import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pokemon } from '../pokemon';
import { PokemonsService } from '../pokemons.service';

@Component({
  selector: 'list-pokemon',
  templateUrl: './list-pokemon.component.html',
  styleUrls: ['./list-pokemon.component.scss']
})

export class ListPokemonComponent implements OnInit {

  private pokemons: Pokemon[];
  private title: string = 'Liste des pokémons';

  constructor(private router: Router, 
              private PS: PokemonsService) {}

  // Initialisation
  ngOnInit() {
    // Toute la logique vient dans l'initialisation surtout pour des données distantes
    // Chargement de tous les pokemons
    this.PS.getPokemons()
    .subscribe(pokemons => this.pokemons = pokemons);

  }

  selectPokemon(pokemon: Pokemon) {
    let link = ['/pokemon', pokemon.id];
    this.router.navigate(link);
  }

}
