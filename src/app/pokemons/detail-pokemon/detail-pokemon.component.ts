import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Pokemon } from '../pokemon';
import { PokemonsService } from '../pokemons.service';

@Component({
  selector: 'app-detail-pokemon',
  templateUrl: './detail-pokemon.component.html',
	styleUrls: ['./detail-pokemon.component.scss']
})
export class DetailPokemonComponent implements OnInit {

	pokemon: Pokemon = null;

	constructor(private route: ActivatedRoute, 
							private router: Router, 
							private PS: PokemonsService) {}

	ngOnInit(): void {
		let id = +this.route.snapshot.params['id'];
		this.PS.getPokemon(id)
		.subscribe(pokemon => this.pokemon = pokemon);
	}

	delete(pokemon): void {
		this.PS.deletePokemon(pokemon)
		.subscribe(_ => this.goBack());
	}

	goBack(): void {
		this.router.navigate(['/pokemon/all']);
	}

	goEdit(pokemon: Pokemon): void {
		this.router.navigate(['/pokemon/edit', pokemon.id]);
	}

}
