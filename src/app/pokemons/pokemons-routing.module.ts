import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailPokemonComponent } from './detail-pokemon/detail-pokemon.component';
import { ListPokemonComponent } from './list-pokemon/list-pokemon.component';
import { EditPokemonComponent } from './edit-pokemon/edit-pokemon.component';
import { AuthGuardGuard } from '../guard/auth-guard.guard';


const pokemonsRoutes: Routes = [
  {
    path: 'pokemon',
    canActivate: [AuthGuardGuard],
    children: [
      { path: 'all', component: ListPokemonComponent },
      { path: 'edit/:id', component: EditPokemonComponent, canActivate: [AuthGuardGuard]},
      { path: ':id', component: DetailPokemonComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(pokemonsRoutes)],
  exports: [RouterModule]
})
export class PokemonRoutingModule { }