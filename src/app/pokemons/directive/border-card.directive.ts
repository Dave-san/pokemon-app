import { Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[appPkmnBorderCard]'
})
export class BorderCardDirective {

  // Déclaration des propriétés
  private initialColor: string = '#f5f5f5';
  private defaultColor: string = '#009688';
  private defaultHeight: number = 180;

  constructor(private el: ElementRef) {
    this.setBorder(this.initialColor);
    this.setHeight(this.defaultHeight);
  }

  // Demande de paramètre
  @Input('appPkmnBorderCard') borderColor: string; // avec alias
  // @Input() appPkmnBorderColor: string;  // sans alias

  // Détecte le mouvement mouse enter
  @HostListener('mouseenter') onMouseEnter() {
    this.setBorder(this.borderColor || this.defaultColor);
  }

  // Détecte le mouvement mouseleave
  @HostListener('mouseleave') onmouseleave() {
    this.setBorder(this.initialColor);
  }

  private setBorder(color: string) {
    let border = 'solid 4px ' + color;
    this.el.nativeElement.style.border = border;
  }

  private setHeight(height: number) {
    this.el.nativeElement.style.height = height + 'px';
  }
}
